from django.db import models

class Profil(models.Model):
    profilname = models.CharField(max_length=255)
    points = models.IntegerField()

class Answer(models.Model):
    q_id = models.IntegerField()
    answer = models.CharField(max_length=255)
    definition = models.CharField(max_length=2550)

class Image(models.Model):
    name = models.IntegerField()
    description = models.CharField(max_length=2550)
    mode = models.CharField(max_length=255)
    celltype = models.CharField(max_length=255)
    component = models.CharField(max_length=255)
    doi = models.CharField(max_length=255)
    organism = models.CharField(max_length=255)

class Questions(models.Model):
    question = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    imagefield = models.CharField(max_length=255)
    point = models.IntegerField()
    q_id = models.IntegerField()
    n_answer = models.IntegerField()
    n_image = models.IntegerField()
