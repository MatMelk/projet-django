from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm


import random as rd

from quizzapp.models import Answer, Questions, Image ,Profil


def home(request,):
    user = request.user
    profil = Profil.objects.get(profilname=user.username)
    context = {
        'user': user,
        'profil': profil
    }
    return render(request, "quizzDJ/home.html", context)


def explore_view(request):
    profil = Profil.objects.get(profilname=request.user.username)
    img = Image.objects.all()
    context={
        'img':img,
        'profil': profil
    }
    return render(request, "quizzDJ/explore.html", context)


def question_view(request,type=1):
    profil = Profil.objects.get(profilname=request.user.username)
    if request.method == "POST":
        profil.points = request.POST['stock']
        profil.save()

    ask = Questions.objects.get(id=type)
    img = Image.objects.get(id=rd.randint(1, 136))
    rep = []
    possible = []
    repv = Answer.objects.get(id=1)
    if ask.id==1:
        repv = Answer.objects.get(answer=img.mode)
    elif ask.id==2:
        repv = Answer.objects.get(answer=img.component)
    for i in Answer.objects.filter(q_id=type).exclude(id=repv.id):
        possible.append(i)
    for i in rd.sample(possible, 3):
        rep.append(i)
    rep.append(repv)
    rd.shuffle(rep)
    context = {
        'ask': ask,
        'img': img,
        'repv': repv,
        'rep':rep,
        'profil': profil
    }
    return render(request, "quizzDJ/question.html", context)

def register_view(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            Profil.objects.create(profilname=request.POST['username'],points=0)
            return redirect("/quizz/login")
    else:
        form = UserCreationForm()

    return render(request, "quizzDJ/register.html", {'form':form})
